## Developer Certificate of Origin + License

By contributing to GitLab B.V., You accept and agree to the following terms and
conditions for Your present and future Contributions submitted to GitLab B.V.
Except for the license granted herein to GitLab B.V. and recipients of software
distributed by GitLab B.V., You reserve all right, title, and interest in and to
Your Contributions. All Contributions are subject to the following DCO + License
terms.

[DCO + License](https://gitlab.com/gitlab-org/dco/blob/master/README.md)

_This notice should stay as the first item in the CONTRIBUTING.md file._

## Adding new language of framework support

`gl-sast` is a wrapper and a parser for existing open source scanners. In order 
to extend library functionality you need next:

1. Choose open source library for your language. 
We recommend you to look at [this list](http://bit.ly/2A0fePf) first.
2. Fork `gl-sast` and add new class to `lib/analyzers` that will do scanning and 
parsing of the output.
3. Submit a merge request to the upstream with code and tests.

## Style guides

- [Ruby](https://github.com/bbatsov/ruby-style-guide)

# gl-sast

[![pipeline status](https://gitlab.com/gitlab-org/gl-sast/badges/master/pipeline.svg)](https://gitlab.com/gitlab-org/gl-sast/commits/master)
[![coverage report](https://gitlab.com/gitlab-org/gl-sast/badges/master/coverage.svg)](https://gitlab.com/gitlab-org/gl-sast/commits/master)

GitLab tool for running Static Application Security Testing (SAST) on provided source code

## How to use

```
# 1. cd into directory with source code you want to test
# 2. Run docker image
docker run \
  --interactive --tty --rm \
  --volume "$PWD":/code \
  registry.gitlab.com/gitlab-org/gl-sast /app/bin/run /code
  
# 3. See result in gl-sast-report.json
cat gl-sast-report.json
```

## Development

Running application

```
# With Docker
docker run \
  --interactive --tty --rm \
  --volume "$PWD":/app \
  --volume /path/to/source/code:/code \
  ruby:latest /app/bin/run /code

# Without Docker (not recommended)
./bin/run /path/to/source/code
```

Running tests

```
bundle install
bundle exec rspec spec
```

## Languages and frameworks support

Next libraries are used by gl-sast to analyze your code

* Ruby - `bundler_audit`
* Rails - `brakeman`
* JavaScript - `retirejs`
* Python - `bandit`

You can help us extend this list. Please see 
'Adding new language of framework support' section in the [contribution guidelines](CONTRIBUTING.md).

# Contributing

Please see the [contribution guidelines](CONTRIBUTING.md)

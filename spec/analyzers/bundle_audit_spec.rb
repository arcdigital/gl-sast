require_relative '../../lib/analyzers/bundle_audit'
require 'spec_helper'

RSpec.describe Analyzers::BundleAudit do
  let!(:app_path) { clone_rails_app }
  let(:app) { double(language: :ruby, framework: :rails, path: app_path) }

  let(:issues) do
    Bundler.with_clean_env do
      Analyzers::BundleAudit.new(app).execute
    end
  end

  it 'expect to have correct issues' do
    expect(issues.size).to eq(1)
    expect(issues.first.tool).to eq(:bundler_audit)
    expect(issues.first.message).to eq('uglifier incorrectly handles non-boolean comparisons during minification')
  end
end

require_relative '../../lib/analyzers/retire'
require 'spec_helper'

RSpec.describe Analyzers::Retire do
  let!(:app_path) { clone_js_app }
  let(:app) { double(language: :js, path: app_path) }

  let(:issues) do
    Bundler.with_clean_env do
      Analyzers::Retire.new(app).execute
    end
  end

  it 'expect to have correct issues' do
    expect(issues.size).to eq(26)
    expect(issues[2].tool).to eq(:retire)
    expect(issues[2].message).to eq('3rd party CORS request may execute')
  end
end

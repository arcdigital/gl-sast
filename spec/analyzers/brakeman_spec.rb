require_relative '../../lib/analyzers/brakeman'
require 'spec_helper'

RSpec.describe Analyzers::Brakeman do
  let!(:app_path) { clone_rails_app }
  let(:app) { double(language: :ruby, framework: :rails, path: app_path) }

  let(:issues) do
    Bundler.with_clean_env do
      Analyzers::Brakeman.new(app).execute
    end
  end

  it 'expect to have correct issues' do
    expect(issues.size).to eq(1)
    expect(issues.first.tool).to eq(:brakeman)
    expect(issues.first.message).to eq('Possible command injection')
  end
end

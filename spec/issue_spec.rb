require_relative '../lib/issue'
require 'spec_helper'

RSpec.describe Issue do
  describe '#to_hash' do
    let(:issue) do
      issue = Issue.new
      issue.url = 'http://example.org'
      issue.message = 'Outdated dependency X'
      issue
    end

    it { expect(issue.to_hash.keys).to contain_exactly('url', 'message') }
    it { expect(issue.to_hash['url']).to eq('http://example.org') }
  end
end

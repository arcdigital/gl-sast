require_relative '../lib/report'
require_relative '../lib/issue'
require 'spec_helper'

RSpec.describe Report do
  describe '#dump' do
    let(:issues) do
      issues = [Issue.new, Issue.new, Issue.new]
      issues[0].priority = 'Low'
      issues[0].message = 'Low priority security issue'
      issues[1].priority = 'High'
      issues[1].message = 'High priority security issue'
      issues[2].priority = nil
      issues[1].message = 'Undefined priority security issue'

      Report.new(issues).dump
    end

    it { expect(JSON[issues][0]['priority']).to eq('High') }
  end
end

require_relative '../lib/app'
require 'spec_helper'

RSpec.describe App do
  let!(:app_path) { clone_rails_app }
  let(:app) { App.new(app_path) }

  it { expect(app.language).to eq(:ruby) }
  it { expect(app.framework).to eq(:rails) }
end

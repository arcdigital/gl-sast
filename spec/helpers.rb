module Helpers
  RAILS_REPO = 'https://gitlab.com/dzaporozhets/sast-sample-rails.git'.freeze
  JS_REPO = 'https://gitlab.com/dz-test-sast/ghost.git'.freeze
  PY_REPO = 'https://gitlab.com/dz-test-sast/django-cms'.freeze

  def git_clone(url, dir)
    path = File.join(File.expand_path(File.dirname(__FILE__)), '../tmp', dir)
    `git clone #{url} #{path}` unless Dir.exist?(path)
    path
  end

  def clone_rails_app
    git_clone(RAILS_REPO, 'rails-app')
  end

  def clone_js_app
    git_clone(JS_REPO, 'js-app')
  end

  def clone_py_app
    git_clone(PY_REPO, 'py-app')
  end
end

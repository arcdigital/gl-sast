require_relative '../lib/analyze'
require 'spec_helper'

RSpec.describe Analyze do
  let!(:app_path) { clone_rails_app }

  context 'not supported language' do
    let(:app) { double(language: :perl, path: app_path) }

    it { expect { Analyze.new(app).issues }.to raise_error(SystemExit) }
  end
end

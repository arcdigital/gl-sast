require 'json'

# Dump issues to json and save as file
class Report
  attr_reader :issues

  def initialize(issues)
    @issues = issues
  end

  def save_as(target_path)
    File.write(target_path, dump)
    puts "SUCCESS: Report saved in #{target_path}"
  end

  def dump
    priority = %w(High Medium Low Unknown).freeze

    @issues.sort_by! { |issue| priority.index(issue.priority) || priority.size }
    @issues.map(&:to_hash).to_json
  end
end
